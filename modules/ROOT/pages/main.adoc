= Página Principal
// :tabs:

Essa é a página principal da documentação. As demais páginas podem ser adicionadas no arquivo `nav.adoc` para que sejam incluídas na barra de navegação.

O arquivo de encontra em `/modules/ROOT/nav.adoc`

.Hello World em Kotlin
[%collapsible]
====
[source, kotlin]
----
fun main(args : Array<String>) {
    println("Hello, World!")
}
----
====

[tabs]
====
Shell::
+
[source,shell]
----
curl localhost:8080
----
Json::
+
[source,json]
----
{
  "teste": "dummy_value",
  "dummy_key": "dummy_value2"
}
----
====

[tabs] 
==== 
Java:: 
+ 
-- 
[source,java]
----
public static void main(String args[]) {
    System.out.println("Teste");
}
----
--

Kotlin::
+
[source,kotlin]
----
fun main(args: Array<String>) {
	println("Teste")
}
---- 
====

[tabs] 
==== 
Tab 1 Title:: 
+ 
-- 
This is the content of the first tab.

Another paragraph in the first tab.

[source,console]
----
$ a code block
----
--

Tab 2 Title::
+
This is the content of the second tab. 
====