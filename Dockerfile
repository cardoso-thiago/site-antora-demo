FROM thiagocardoso33/alpine-user:0.0.1

USER root

ENV DOCSEARCH_ENABLED=true \
    DOCSEARCH_ENGINE=lunr

RUN apk --no-cache add nodejs yarn --repository=http://dl-cdn.alpinelinux.org/alpine/edge/community
RUN yarn config set strict-ssl false
RUN yarn global add @antora/cli@2.3
RUN yarn global add @antora/site-generator-default@2.3
RUN yarn global add antora-site-generator-lunr
RUN apk add jpegoptim
RUN apk add optipng
RUN apk add imagemagick
RUN apk add pngcrush
RUN apk add rsync
RUN apk add perl