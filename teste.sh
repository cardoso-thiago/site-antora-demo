#!/bin/sh

LATEST_VERSION=$1
REDIRECT_VERSION=$2

echo "Vai pegar as páginas da versão $LATEST_VERSION como base."
echo "Vai gerar as páginas de redirect para a versão $REDIRECT_VERSION"

for FILE in  $(find public -path "*$LATEST_VERSION/*.html")
do
URL_PATH="$( echo "$FILE" | sed -e 's#^public/##;' )"
NEW_FILE="$( echo "$FILE" | sed -e "s#\\$LATEST_VERSION#$REDIRECT_VERSION#" )"
echo $NEW_FILE
mkdir -p "$(dirname "$NEW_FILE")" && touch "$NEW_FILE"
cat > $NEW_FILE << ENDOFFILE
<html>
<head>
    <meta charset="utf-8"/>
    <title>Redirecionando</title>
    <meta http-equiv="refresh" content="0; URL='https://cardoso-thiago.gitlab.io/site-antora-demo/$URL_PATH'"/>
</head>
<body>
Redirecionando para a uma versão atualizada.
</body>
</html>
ENDOFFILE
done